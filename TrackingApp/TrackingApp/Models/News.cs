﻿using Newtonsoft.Json;
using System;
using TrackingApp.Services;

namespace TrackingApp.Models
{
	public class News
	{
		[JsonProperty("newsId")]
		public int Id { get; set; }

		[JsonProperty("newsDate")]
		public DateTime DateTime { get; set; }
		public string DateTimeString { get; set; } 

		private string _Text;

		[JsonProperty("newsContent")]
		public string Text {
			get { return this._Text; }
			set { this._Text = value; }
		}

		[JsonProperty("newsPicture")]
		public string B64NewsPicture { get; set; }

		[JsonProperty("newsHeader")]
		public string NewsHeader { get; set; }

		//TODO muss nochmal reviewed werden... ggf. fehlen noch attribute
		public News(int id, DateTime DateTime, String content, String header, String picture)
		{
			this.Id = id;
			this.DateTime = DateTime;
			this.DateTimeString = DateTime.ToString("D",Constants.CultureInfo);
			this.Text = content;
			this.NewsHeader = header;
			this.B64NewsPicture = picture;
		}


	}
}

