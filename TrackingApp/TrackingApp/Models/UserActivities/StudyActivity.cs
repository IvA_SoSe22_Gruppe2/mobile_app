﻿using System;
namespace TrackingApp.Models.UserActivities
{
    public class StudyActivity : UserActivity
    {
        public StudyActivity(int id, int typeId, int userId, DateTime startDate, DateTime endDate) : base(id, typeId, userId, startDate, endDate)
        {

        }
    }
}

