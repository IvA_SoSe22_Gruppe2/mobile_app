﻿using System;
using Newtonsoft.Json;
using TrackingApp.Models.Ressources;
using TrackingApp.Services;
using Xamarin.Forms;

namespace TrackingApp.Models.UserActivities
{
    public class RunActivity : UserActivity
    {
        //TODO Steps, wenn Aufzeichnen über Sensor läuft
        // für Vleocity & Calories:
        // https://www.runners-flow.de/kalorienverbrauch-joggen/

        [JsonProperty("DistanceInKM")]
        public new double DistanceInKM { get; set; }
        [JsonProperty("AvgSpeed")]
        public double AvgSpeed { get; set; }
        [JsonProperty("Calories")]
        public double Calories { get; set; }
 


        public RunActivity(int id, int typeId, int userId, DateTime startDate, DateTime endDate, double distanceInKM) : base(id, typeId, userId, startDate, endDate)
        {
            DistanceInKM = distanceInKM;
            DistanceString = GetDistanceString(distanceInKM);
            AvgSpeed = distanceInKM / Duration.TotalHours;
        }
    }
}

