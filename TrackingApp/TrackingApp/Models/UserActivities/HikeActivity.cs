﻿using System;
using Newtonsoft.Json;
using TrackingApp.Models.Ressources;
using TrackingApp.Services;
using Xamarin.Forms;

namespace TrackingApp.Models.UserActivities
{
    public class HikeActivity : UserActivity
    {
        [JsonProperty("DistanceInKM")] 
        public new double DistanceInKM { get; set; }
        [JsonProperty("AvgSpeed")]
        public double AvgSpeed { get; set; }
        [JsonProperty("Calories")]
        public double Calories { get; set; }

        //https://www.omnicalculator.com/sports/hiking

        public HikeActivity(int id, int typeId, int userId, DateTime startDate, DateTime endDate, double distanceInKM) : base(id, typeId, userId ,startDate, endDate)
        {
            DistanceInKM = distanceInKM;
            DistanceString = GetDistanceString(distanceInKM);
            AvgSpeed = distanceInKM / Duration.TotalHours;
        }
    }
}

