﻿using System;
namespace TrackingApp.Models.UserActivities
{
    public class WorkActivity : UserActivity
    {
        public WorkActivity(int id, int typeId, int userId, DateTime startDate, DateTime endDate) : base(id, typeId, userId, startDate, endDate)
        {

        }
    }
}

