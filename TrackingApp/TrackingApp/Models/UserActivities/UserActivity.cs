﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Newtonsoft.Json;
using TrackingApp.Services;
using Xamarin.Forms;

namespace TrackingApp.Models.UserActivities
{
    public abstract class UserActivity
    {

        [JsonIgnore]
        public ImageSource ImageSource { get; set; }
        [JsonProperty("eventID")]
        public int Id { get; set; }
        [JsonProperty("activityID")]
        public int TypeId { get; set; }
        [JsonProperty("userID")] 
        public int UserId { get; set; }
        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }
        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }
        [JsonIgnore]
        public TimeSpan Duration { get; set; }
        [JsonProperty("duration")] 
        public long DurationAsLong { get; set; }

        [JsonIgnore]
        public double DistanceInKM { get; set; }

        [JsonIgnore]
        public string DistanceString { get; set; }

        [JsonIgnore]
        public ActivityType ActivityType { get; set; }

        [JsonIgnore]
        public string DashboardString { get; set; }
        [JsonIgnore]
        public string DurationString { get; set; }



        public UserActivity(int id, int typeId, int userId, DateTime startDate, DateTime endDate)
        {
            Id = id;
            TypeId = typeId;
            UserId = userId;
            StartDate = startDate;
            EndDate = endDate;
            Duration = endDate - startDate;
            DurationAsLong = Duration.Ticks;
            //TimeSpan myDuration = Duration.Subtract(TimeSpan.FromMilliseconds(Duration.TotalMilliseconds));
            //DurationString = myDuration.ToString("g",Constants.CultureInfo); 
            DurationString = Duration.ToString(@"hh\h\ mm\m", Constants.CultureInfo);
            ImageSource = "ActivityIconPlaceHolder";
            ActivityType = Constants.GetActivityTypeByID(typeId);
            DashboardString = $"{startDate.ToString("g",Constants.CultureInfo)} - {endDate.ToString("t",Constants.CultureInfo)}" ;
         }


        protected string GetDistanceString(double distance)
        {
            return $"{distance} km";
        }

    }
}
