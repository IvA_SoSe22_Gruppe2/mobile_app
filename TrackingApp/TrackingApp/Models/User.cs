﻿using Newtonsoft.Json;
using System;
using TrackingApp.Models.Ressources;

namespace TrackingApp.Models
{
	public class User
	{
        [JsonProperty("userId")]
        public int Id { get; set; }

        [JsonProperty("firstName")]
        public string Surname { get; set; }

        [JsonProperty("lastName")]
        public string Lastname { get; set; }
 
        [JsonProperty("gender")]
        public string GenderString { get; set; }

        [JsonProperty("height")]
        public int HeightInCm { get; set; }

        [JsonProperty("age")]
        public DateTime Birthdate { get; set; }

        [JsonIgnore]
        public int Age { get; set; }

        [JsonProperty("weight")]
        public int WeightInKG { get; set; }

        [JsonProperty("profilePicture")]
        public string Base64ImageRepresentation { get; set; }



  
        public User(int id, string surname, string lastname, string gender, int heightInCm, DateTime age, int weightInKG, string base64ImageRepresentation)
        {
            Id = id;
            Surname = surname ?? throw new ArgumentNullException(nameof(surname));
            Lastname = lastname ?? throw new ArgumentNullException(nameof(lastname));
            //Gender = gender;
            HeightInCm = heightInCm;
            Birthdate = age;
            WeightInKG = weightInKG;
            Age = CalculateAge();
            Base64ImageRepresentation = base64ImageRepresentation;
            GenderString = gender;
        }

  


        // From: https://www.c-sharpcorner.com/code/961/how-to-calculate-age-from-date-of-birth-in-c-sharp.aspx
        // Calculates Age with Birthdate
        public int CalculateAge()
        {
            int Age = DateTime.Now.Year - Birthdate.Year;
            if (DateTime.Now.DayOfYear < Birthdate.DayOfYear)
                Age --;

            return Age;
        }

    }
}

