﻿using System;
using System.Runtime.Serialization;

namespace TrackingApp.Models.UserActivities
{
    [Serializable]
    internal class ActivityTypeNotFoundException : Exception
    {
        private readonly int activityId;

        public ActivityTypeNotFoundException()
        {
        }

        public ActivityTypeNotFoundException(int activityId)
        {
            this.activityId = activityId;
        }

        public ActivityTypeNotFoundException(string message) : base(message)
        {
        }

        public ActivityTypeNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ActivityTypeNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
        public override string Message
        {
            get
            {
                return $"Activity Type with ID: {activityId} not Found!";
            }
        }
    }
}