﻿using System;
using System.Runtime.Serialization;

namespace TrackingApp.Models.UserActivities
{
    [Serializable]
    internal class CalorieCalculationException : Exception
    {
        private readonly double avgSpeed;

        public CalorieCalculationException()
        {
        }

        public CalorieCalculationException(double avgSpeed)
        {
            this.avgSpeed = avgSpeed;
        }

        public CalorieCalculationException(string message) : base(message)
        {
        }

        public CalorieCalculationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CalorieCalculationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public override string Message
        {
            get 
            { 
                return $"Error while calculating Calories with avg. speed {avgSpeed}"; 
            }
        }
    }
}