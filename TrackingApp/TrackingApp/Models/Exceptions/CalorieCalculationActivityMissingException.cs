﻿using System;
using System.Runtime.Serialization;

namespace TrackingApp.Models.Ressources
{
    [Serializable]
    internal class CalorieCalculationActivityMissingException : Exception
    {
        private int activityID;

        public CalorieCalculationActivityMissingException()
        {
        }

        public CalorieCalculationActivityMissingException(int activityID)
        {
            this.activityID = activityID;
        }

        public CalorieCalculationActivityMissingException(string message) : base(message)
        {
        }

        public CalorieCalculationActivityMissingException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CalorieCalculationActivityMissingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
        public override string Message
        {
            get
            {
                return $"No Calorie Calculation Available for AcivityID: {activityID}";
            }
        }
    }
}