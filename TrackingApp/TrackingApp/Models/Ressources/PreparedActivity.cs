﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using TrackingApp.Models.UserActivities;
using Xamarin.Forms;

namespace TrackingApp.Models.Ressources
{
    public class PreparedActivity
    {
        [JsonProperty("activityID")] // wandern, laufen, arbeiten, lernen, ...
        public int TypeId { get; set; }

        [JsonProperty("userID")] // Fremdschlüssel
        public int UserId { get; set; }

        [JsonProperty("startDate")]
        public DateTime StartDate { get; set; }
        [JsonProperty("endDate")]
        public DateTime EndDate { get; set; }
        [JsonIgnore]
        public TimeSpan Duration { get; set; }
        [JsonProperty("duration")] // sollten wir speichern, auch wenn einfach zu berechnen 
        public long DurationAsLong { get; set; }

        [JsonProperty("distanceInKm")]
        public double DistanceInKM { get; set; }
        [JsonProperty("avgSpeed")]
        public double AvgSpeed { get; set; }
        [JsonProperty("calories")]
        public double Calories { get; set; }




        public PreparedActivity(int typeId, int userId, DateTime startDate, DateTime endDate, double distanceInKM)
        {
            TypeId = typeId;
            UserId = userId;
            StartDate = startDate;
            EndDate = endDate;
            Duration = endDate - startDate;
            DurationAsLong = Duration.Ticks;
            DistanceInKM = distanceInKM;
            AvgSpeed = distanceInKM / Duration.TotalHours;
            try
            {
                Calories = CaloriesCalculator.GetCalories(typeId, Duration, AvgSpeed);
            }
            catch (CalorieCalculationException ex)
            {
                Application.Current.MainPage.DisplayAlert("Fehler", ex.Message, "OK");
            }
        }

    }
}
