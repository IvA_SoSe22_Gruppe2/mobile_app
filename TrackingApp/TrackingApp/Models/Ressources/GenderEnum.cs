﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrackingApp.Models.Ressources
{
    public enum GenderEnum
    {
        Male,
        Female,
        Other,
        Unknown
    }

    public static class GenderEnumExtensions
    {
        public static string ToString(GenderEnum gender)
        {
            return gender switch
            {
                GenderEnum.Male => "männlich",
                GenderEnum.Female => "weiblich",
                GenderEnum.Other => "divers",
                GenderEnum.Unknown => "Unbekannt",
                _ => "Fehler!",
            };
        }

        internal static GenderEnum GetGenderByString(string gender)
        {
            throw new NotImplementedException();
        }
    }
}
