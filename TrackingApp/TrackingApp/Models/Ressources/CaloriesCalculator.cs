﻿using System;
using System.Collections.Generic;
using System.Text;
using TrackingApp.Models.UserActivities;
using TrackingApp.Services;

namespace TrackingApp.Models.Ressources
{
    public static class CaloriesCalculator
    {

        public static double GetCalories(int activityID, TimeSpan duration, double avgSpeed)
        {
            switch (Constants.GetActivityTypeByID(activityID).Name)
            {
                case "Wandern": return 6 * Constants.GlobalUser.WeightInKG * duration.TotalHours;
                case "Laufen": {
                        return avgSpeed switch
                        {
                            double n when (n < 10) => 8 * Constants.GlobalUser.WeightInKG * duration.TotalHours,
                            double n when (n >= 10 && n < 12) => 10 * Constants.GlobalUser.WeightInKG * duration.TotalHours,
                            double n when (n >= 10) => 12 * Constants.GlobalUser.WeightInKG * duration.TotalHours,
                            _ => throw new CalorieCalculationException(avgSpeed),
                        };
                    }
                default: return 0;

            }
        }

    }
}
