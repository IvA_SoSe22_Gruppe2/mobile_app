﻿using System; 
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Linq;
using TrackingApp.Models;
using Xamarin.Forms;
using System.ComponentModel;
using TrackingApp.Services;


namespace TrackingApp.ViewModels
{
	public class ActivityViewModel : ViewModelBase
    {
		public ObservableCollection<ActivityType> Activities { get; set; }

		public ActivityViewModel()
		{
           Activities = Constants.ActivityTypes;
        }
	}
}

