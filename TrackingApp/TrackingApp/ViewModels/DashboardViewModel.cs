﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using TrackingApp.Models;
using TrackingApp.Models.UserActivities;
using TrackingApp.Services;
using Xamarin.Forms;
using Microcharts;
using Entry = Microcharts.ChartEntry;
using System.Threading.Tasks;

namespace TrackingApp.ViewModels
{
	public class DashboardViewModel : ViewModelBase
	{
		public string DisplayName { get; set; }
        public RestService _restService;

		public ObservableCollection<News> News { get; set; }
		public ObservableCollection<UserActivity> Activities { get; set; }

		public double AverageCalories { get; set; }
		public string AverageCaloriesText { get; set; }
		public double AverageSpeed { get; set; }
		public string AverageSpeedText { get; set; }
		public int[] AbsoluteAmountsOfTrainingsByActivityType { get; set; }

		public Command RefreshCommand { get; set; }
		private bool isRefreshing;
		public bool IsRefreshing
		{
			get => isRefreshing;
			set => SetProperty(ref isRefreshing, value);
		}
		public List<Entry> entries = new List<Entry>();

		public Chart DashboardChart { get; set; }

		public string[] ColorCodes { get; set; }

		public DashboardViewModel()
		{
			_restService = new RestService();

			RefreshCommand = new Command(() => OnRefreshAsync());

			Console.WriteLine("Jetzt im Dashboard");
			DisplayName = $"Hallo {Constants.GlobalUser.Surname}!";
			
			Activities = new ObservableCollection<UserActivity>();
            Activities = Constants.Activities;
			if(Activities != null) Activities.OrderBy(i => i.StartDate);

			LoadDiagrammData();

			News = new ObservableCollection<News>();
			Console.WriteLine("Ordne News");
			Constants.NewsList.OrderBy(i => i.DateTime);
			Console.WriteLine("Hole die ersten 2 News");

			//News = GetFirstNNews(Constants.NewsList, 2);
            if (Constants.NewsList != null)
            {
				News = Constants.NewsList;
			}
			

			OnPropertyChanged(nameof(News));

			GenerateDashboardCharts();
			OnPropertyChanged(nameof(DashboardChart));

		}

        private void LoadDiagrammData()
        {
			Constants.LoadDiagrammData();
			AverageCalories = Math.Round(Constants.AverageCalories, 2);
			AverageSpeed = Math.Round(Constants.AverageSpeed, 2);
			AbsoluteAmountsOfTrainingsByActivityType = Constants.AbsoluteAmountsOfTrainingsByActivityType;

			AverageCaloriesText = "ø Kalorienverbrauch: " + AverageCalories + " kcal";
			AverageSpeedText = "ø Geschwindigkeit: " + AverageSpeed + " km/h";

			OnPropertyChanged(nameof(AverageCalories));
			OnPropertyChanged(nameof(AverageSpeed));
			OnPropertyChanged(nameof(AverageCaloriesText));
			OnPropertyChanged(nameof(AverageSpeedText));
			OnPropertyChanged(nameof(AbsoluteAmountsOfTrainingsByActivityType));

		}

		private void GenerateDashboardCharts()
        {
			ColorCodes = new string[5] { "#D35400", "#F1C40F", "#E74C3C", "#2980B9", "#27AE60" };
			int i = 0;
			entries.Clear();
			foreach (ActivityType activityType in Constants.ActivityTypes)
			{
				entries.Add((new Entry(AbsoluteAmountsOfTrainingsByActivityType[activityType.Id - 1])
				{
					Label = activityType.Name,
					ValueLabel = AbsoluteAmountsOfTrainingsByActivityType[activityType.Id - 1].ToString(),
					Color = SkiaSharp.SKColor.Parse(ColorCodes[i]),
					ValueLabelColor = SkiaSharp.SKColor.Parse(ColorCodes[i]),
				}));
				i++;
			}

			DashboardChart = new DonutChart() { Entries = entries, LabelMode = LabelMode.RightOnly, LabelTextSize = 20, Margin = 15 };

		}

        // TODO pruefen ob wir das so noch brauchen, da wir oben schon OrderBy machen.. müssten also nur Objekt 0 und 1 in eine Collection schreiben
        private ObservableCollection<News> GetFirstNNews(ObservableCollection<News> newsList, int N)
        {
			ObservableCollection<News> result = new ObservableCollection<News>();
			int counter = 0;

			newsList.OrderBy(i => i.DateTime);
			foreach (News item in newsList)
            {
				result.Add(item);
				counter++;

				if (counter == N)
                {
					return result;
                }
            }

			return result;
        }

		private async void OnRefreshAsync()
		{
			IsRefreshing = true;
			OnPropertyChanged(nameof(IsRefreshing));
			Activities = Constants.Activities;
			if (Activities != null)
			{
				Activities = new ObservableCollection<UserActivity>(Constants.Activities.OrderByDescending (i => i.StartDate));
			}

			OnPropertyChanged(nameof(Activities));
			Constants.NewsList = await _restService.GetAllNews();
			News = new ObservableCollection<News>();
			Console.WriteLine("Ordne News");
			Constants.NewsList.OrderBy(i => i.DateTime);
			Console.WriteLine("Hole die ersten 2 News");

			//News = GetFirstNNews(Constants.NewsList, 2);
			if (Constants.NewsList != null)
			{
				News = Constants.NewsList;
			}


			OnPropertyChanged(nameof(News));

			LoadDiagrammData();
			GenerateDashboardCharts();
			OnPropertyChanged(nameof(DashboardChart));
			IsRefreshing = false;
			OnPropertyChanged(nameof(IsRefreshing));

			return;
		}

        private Command pageAppearingCommand;
        public ICommand PageAppearingCommand => pageAppearingCommand ??= new Command(PageAppearing);

        private void PageAppearing()
        {
			OnRefreshAsync();
        }
    }
}

