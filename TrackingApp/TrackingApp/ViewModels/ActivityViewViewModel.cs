﻿using System;
using System.Collections.ObjectModel;
using System.Linq; 
using System.Windows.Input;
using TrackingApp.Models;
using TrackingApp.Models.Ressources;
using TrackingApp.Models.UserActivities;
using TrackingApp.Services;
using TrackingApp.Views;
using Xamarin.Forms;

namespace TrackingApp.ViewModels
{

    [QueryProperty("ActivityID", "activityTypeId")]
	//[QueryProperty(nameof(ActivityID), nameof(ActivityID))]
	[QueryProperty("StartTimeTicks", "startTime")]
	[QueryProperty("EndTimeTicks", "endTime")]
	public class ActivityViewViewModel : ViewModelBase
	{
		public DateTime SelectedDate { get; set; }
		public TimeSpan StartTime { get; set; }
		public TimeSpan EndTime { get; set; }

		public double Distance { get; set; }

		private RestService RestService { get; set; }
		public bool DistanceBool { get; set; }

		public int ActivityIDint { get; set; }

		public string UserDistanceEntry { get; set; }

		public string aID = "";
		public string ActivityID {
			get => aID;
			set
			{
				aID = Uri.UnescapeDataString(value ?? string.Empty);
				OnPropertyChanged();
				CsActivityViewViewModel(Int32.Parse(value));
			}
		}
		public string sTT = "";
		public string StartTimeTicks
		{
			get => sTT;
			set
			{
				sTT = Uri.UnescapeDataString(value ?? string.Empty);
				OnPropertyChanged();
				StartTime = new DateTime(long.Parse(value)).TimeOfDay;
				OnPropertyChanged(nameof(StartTime));
			}
		}
		public string eTT = "";
		public string EndTimeTicks
		{
			get => eTT;
			set
			{
				eTT = Uri.UnescapeDataString(value ?? string.Empty);
				OnPropertyChanged();
				EndTime = new DateTime(long.Parse(value)).TimeOfDay;
				OnPropertyChanged(nameof(EndTime));
			}
		}
		public ActivityType SelectedActivityType { get; set; }
		public string SelectedActivityTypeName { get; set; }

		public string PageString { get; set; }



		public void CsActivityViewViewModel(int actId)
		{
			
			Distance = 0;
			RestService = new RestService();
			ActivityIDint = actId;

			SelectedActivityType = Constants.GetActivityTypeByID(ActivityIDint);

			SelectedActivityTypeName = SelectedActivityType.Name;
			PageString = $"{SelectedActivityTypeName} nacherfassen:";
			OnPropertyChanged(nameof(PageString));
			OnPropertyChanged(nameof(SelectedActivityTypeName));

			UserDistanceEntry = "0";
			OnPropertyChanged(nameof(UserDistanceEntry));

			DistanceBool = SelectedActivityType.Name switch
			{
				"Wandern" => true,
				"Laufen" => true,
				_ => false,
			};
			OnPropertyChanged(nameof(DistanceBool));
			
            if (StartTimeTicks == "")
            {
				StartTime = new TimeSpan();
				EndTime = new TimeSpan();
				OnPropertyChanged(nameof(StartTime));
				OnPropertyChanged(nameof(EndTime));
			}
			SelectedDate = DateTime.Now;
			OnPropertyChanged(nameof(SelectedDate));
		}

        private Command closePageCommand;
        public ICommand ClosePageCommand => closePageCommand ??= new Command(ClosePage);
        private void ClosePage()
        {
			Shell.Current.GoToAsync($"//{nameof(ActivityPage)}");
		}


        private Command saveActivity;
        public ICommand SaveActivity => saveActivity ??= new Command(PerformSaveActivity);

        private async void PerformSaveActivity()
        {
			DateTime startTime = GetStartTime();
			DateTime endTime = GetEndTime();
			Distance = double.Parse(UserDistanceEntry);

			PreparedActivity preparedActivity = new PreparedActivity(ActivityIDint,Constants.GlobalUser.Id, startTime, endTime, Distance);
			bool success = await RestService.AddActivitiy(preparedActivity);
			Constants.Activities = await RestService.GetAllActivities();

			// TODO Idee: hier erst nach ID sortieren, um neuste Aktivität zu bekommen?
			// Frage: Müssen wir die Collection danach wieder nach StartDate sortieren? Brauchen wir das danach irgendwo?
			if (success)
			{
				UserActivity savedActivity = Constants.Activities.OrderBy(i => i.Id).Last();
				// send User to ActivityDetailsPage with ID of most recently added activity
				await Shell.Current.GoToAsync($"//{nameof(ActivityPage)}");
				Shell.Current.CurrentItem.CurrentItem = Shell.Current.CurrentItem.Items[0];
				await Shell.Current.GoToAsync($"{nameof(ActivityDetailsPage)}?activityId={savedActivity.Id}");
				Constants.Activities = new ObservableCollection<UserActivity>(Constants.Activities.OrderBy(i => i.StartDate));
			} else
            {
				await Application.Current.MainPage.DisplayAlert("Fehler!", "Aktivität konnte nicht erfasst werden, bitte prüfen Sie Ihre Eingaben.", "OK");
			}
			

			//ClosePage();
		}
		private DateTime GetStartTime()
		{
			DateTime result;

			result = SelectedDate;
			result = result.Add(StartTime);

			return result;
		}

		private DateTime GetEndTime()
        {
			DateTime result;

			result = SelectedDate;
			if (EndTime < StartTime) result = result.AddDays(1);
			result = result.Add(EndTime);

			return result;
		}


    }
}

