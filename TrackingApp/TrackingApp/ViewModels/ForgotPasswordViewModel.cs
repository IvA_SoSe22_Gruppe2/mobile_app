﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using TrackingApp.Services;
using Xamarin.Forms;

namespace TrackingApp.ViewModels
{
	public class ForgotPasswordViewModel : ViewModelBase
	{
		public ForgotPasswordViewModel()
		{
            restService = new RestService();
        }

        readonly RestService restService;

        private Command forgotPassword;
        public ICommand ForgotPassword => forgotPassword ??= new Command(PerformForgotPassword);

        private async void PerformForgotPassword()
        {
            bool result = await restService.ForgotPasswordAsync(EMail);

            if (result)
            {
                await Application.Current.MainPage.DisplayAlert("Anfrage versendet", "Ein Administrator hat deine Anfrage erhalten und wird sich mit dir in Verbindung setzen.", "OK");
            } else
            {
                await Application.Current.MainPage.DisplayAlert("Login nicht erfolgreich", "Etwas ist schiefgelaufen. Bitte versuche es später erneut", "OK");
            }
        }

  
         public string EMail { get; set; }
    }
}

