﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using TrackingApp.Models;
using TrackingApp.Models.Ressources;
using TrackingApp.Models.UserActivities;
using TrackingApp.Services;
using TrackingApp.Views;
using Xamarin.Forms;

namespace TrackingApp.ViewModels
{
    public class LoginViewModel : ViewModelBase
	{
        readonly RestService restService;

		public LoginViewModel()
		{
            restService = new RestService(); 
		}

        public string UserEMail { get; set; }
        public string UserPassword { get; set; }

        private Command forgotPasswordCommand;
        public ICommand ForgotPasswordCommand => forgotPasswordCommand ??= new Command(ForgotPassword);

        private async void ForgotPassword()
        {
            var route = $"{nameof(ForgotPasswordPage)}";
            await Shell.Current.GoToAsync(route);
        }

        private Command loginUser;
        public ICommand LoginUser => loginUser ??= new Command(PerformLoginUser);

        private Command loginBypass;
        public ICommand LoginBypass => loginBypass ??= new Command(PerformLoginBypass);

        private async void PerformLoginBypass(object obj)
        {
            User user = await restService.GetUserByID(4);
            Constants.GlobalUser = user;
            await FetchDashboardData();
            var route = $"//{nameof(DashboardPage)}";
            await Shell.Current.GoToAsync(route);
        }

        private async void PerformLoginUser()
        {

            /*DEMO DATA
            DateTime date = new DateTime(1995, 6, 29);
            User user = new User(25, "Tom", "Steuber", GenderEnum.Male, 180, date, 75, ImageAsStringInB64);
            user = await restService.GetUserByID(4);
            Constants.GlobalUser = user;*/

            int id = await restService.LoginUser(UserEMail, UserPassword);
            if (id == -1)
            {
                await Application.Current.MainPage.DisplayAlert("Login nicht erfolgreich", "Die eingebenenen Daten sind inkorrekt, oder der User ist nicht aktiviert.", "OK");
            }
            else
            {
                
                //User user = await restService.GetUserByID(id);
                //Constants.GlobalUser = user;
                await FetchDashboardData();
                Console.WriteLine("Baue Route");
                var route = $"//{nameof(DashboardPage)}";
                await Shell.Current.GoToAsync(route);
            }

        }

        // Load all data for display on dashboard after successful login
        private async Task<bool> FetchDashboardData()
        { 
            //load all news items from database into Constants data store
            
            Console.WriteLine("Hole News");
            Constants.NewsList = await restService.GetAllNews();
            Console.WriteLine("News sind gespeichert");

            //load all ActivityTypes into Constants

            Console.WriteLine("Hole ActivityTypes");
            Constants.ActivityTypes = await restService.GetAllActivityTypes();
            Console.WriteLine("ActivityTypes sind gespeichert");

            //load all activities into Constants

            Console.WriteLine("Hole Activities");
            Constants.Activities = await restService.GetAllActivities();
            Constants.Activities = new ObservableCollection<UserActivity>(Constants.Activities.OrderByDescending(i => i.StartDate));
            Console.WriteLine("Activities sind gespeichert");

            return true;
            
        }

    }
}

