﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Input;
using TrackingApp.Models;
using TrackingApp.Services;
using TrackingApp.Views;
using Xamarin.Forms;

namespace TrackingApp.ViewModels
{
    [QueryProperty("ActivityID", "activityTypeId")]
	public class ActivityRecordViewModel : ViewModelBase
	{

		public string aID = "";
		public string ActivityID
		{
			get => aID;
			set
			{
				aID = Uri.UnescapeDataString(value ?? string.Empty);
				OnPropertyChanged();
				CsActivityRecordViewModel(Int32.Parse(value));
			}
		}

		private int ActivityIDint { get; set; }
		public ActivityType SelectedActivityType { get; set; }

        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public string PageString { get; set; }
        public string PopUpString { get; set; }

        public bool IsRunning { get; set; }
        public void CsActivityRecordViewModel(int actId)
		{
            IsRunning = false;
            OnPropertyChanged(nameof(IsRunning));
            ActivityIDint = actId;
			SelectedActivityType = Constants.GetActivityTypeByID(ActivityIDint);
            PageString = $"{SelectedActivityType.Name} aufzeichnen";
            OnPropertyChanged(nameof(PageString));
		}

        public ActivityRecordViewModel()
        {
        }

   

        

        private Command startCommand;
        public ICommand StartCommand => startCommand ??= new Command(Start);

        private Command stopCommand;
        public ICommand StopCommand => stopCommand ??= new Command(Stop);


        private void Start()
        {
            StartTime = DateTime.Now;
            IsRunning = true;
            OnPropertyChanged(nameof(IsRunning));
        }
        private async void Stop()
        {
            //TODO
            /*
             * DateTime erfassen
             * PoPUp mit Summary
             * Bei OK Uebertragen
             * Bei nOk zurueck
             */
            EndTime = DateTime.Now;
            BuildString();
            bool answer = await Application.Current.MainPage.DisplayAlert($"{SelectedActivityType.Name}: Zeit aufgezeichnet!", $"{PopUpString}", "Bestätigen","Abbrechen");

            if (answer)
            {
                await Shell.Current.GoToAsync($"{nameof(ActivityViewPage)}?activityTypeId={SelectedActivityType.Id}&startTime={StartTime.Ticks}&endTime={EndTime.Ticks}");
            }
            else
            {
                IsRunning = false;
                OnPropertyChanged(nameof(IsRunning));
            }
        }

        private void BuildString()
        {
            PopUpString = $"Start: {StartTime.ToString("g", Constants.CultureInfo)}\nEnde: {EndTime.ToString("t", Constants.CultureInfo)}";
            OnPropertyChanged(nameof(PopUpString));
        }

        private Command closePageCommand;
        public ICommand ClosePageCommand => closePageCommand ??= new Command(ClosePage);

        private void ClosePage()
        {
            Shell.Current.GoToAsync("..");
        }

    }
}

