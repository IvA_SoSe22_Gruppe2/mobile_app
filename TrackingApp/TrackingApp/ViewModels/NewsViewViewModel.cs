﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Input;
using TrackingApp.Models;
using TrackingApp.Models.Ressources;
using TrackingApp.Services;
using Xamarin.Forms;

namespace TrackingApp.ViewModels
{
	[QueryProperty("NewsID", "newsId")]

	public class NewsViewViewModel : ViewModelBase
	{
		public string nID = "";
		public string NewsDateText { get; set; }
		public string NewsContent { get; set; }
		public string NewsHeader { get; set; }
		public string NewsID
		{
			get => nID;
			set
			{
				nID = Uri.UnescapeDataString(value ?? string.Empty);
				FetchSelectedNewsData(Int32.Parse(value));
				//OnPropertyChanged();
			}
		}

		public void FetchSelectedNewsData(int newsId)
        {
			foreach(News news in Constants.NewsList)
            {
				if(news.Id == newsId)
                {
					NewsDateText = "veröffentlicht am " + news.DateTimeString;
					NewsContent = news.Text;
					NewsHeader = news.NewsHeader;
					OnPropertyChanged(nameof(NewsHeader));
					OnPropertyChanged(nameof(NewsDateText));
					OnPropertyChanged(nameof(NewsContent));
					return; 
                }
            }
        }

		private Command closePageCommand;
		public ICommand ClosePageCommand => closePageCommand ??= new Command(ClosePage);
		private void ClosePage()
		{
			Shell.Current.GoToAsync("..");
		}

		/*public NewsViewViewModel()
		{

		}*/

	}
}

