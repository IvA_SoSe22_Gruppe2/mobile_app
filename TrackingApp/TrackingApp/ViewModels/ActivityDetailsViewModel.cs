﻿using System;
using System.Windows.Input;
using TrackingApp.Models.UserActivities;
using TrackingApp.Services;
using TrackingApp.Views;
using Xamarin.Forms;

namespace TrackingApp.ViewModels
{
    [QueryProperty("ActivityID", "activityId")]
	public class ActivityDetailsViewModel : ViewModelBase
	{
		private string aID = "";
		public string ActivityID
		{
			get => aID;
			set
			{
				aID = Uri.UnescapeDataString(value ?? string.Empty);
				OnPropertyChanged(nameof(aID));
				FetchSelectedActivity(Int32.Parse(value));
			}
		}
		public UserActivity Activity { get; set; }

		public string DateString { get; set; }
		public string TimeString { get; set; }
		public string KmString { get; set; }
		public string DurationString { get; set; }
		public string AverageVelocityString { get; set; }
		public string CaloriesString { get; set; }


		public void FetchSelectedActivity(int activityId)
        {
			foreach (UserActivity userActivity in Constants.Activities)
            {
				if(userActivity.Id == activityId)
                {
					Activity = userActivity;
					OnPropertyChanged(nameof(Activity));
					GenerateStrings();
					return;
                }
            }
        }

        private void GenerateStrings()
        {
			

			DateString = Activity.StartDate.ToString("D", Constants.CultureInfo);
			TimeString =$"{Activity.StartDate.ToString("t", Constants.CultureInfo)} - {Activity.EndDate.ToString("t", Constants.CultureInfo)}";
			

			DurationString = "";
			if (Activity.Duration.Hours > 0) DurationString += $"{Activity.Duration.Hours} Stunden \n";
			if (Activity.Duration.Minutes > 0) DurationString += $"{Activity.Duration.Minutes} Minuten \n";
			if (Activity.Duration.Seconds > 0) DurationString += $"{Activity.Duration.Seconds} Sekunden";

            if (Activity is HikeActivity hikeActivity)
            {
				double speed = Math.Truncate(hikeActivity.AvgSpeed * 100) / 100;
				AverageVelocityString = $"{speed} km/h";
				CaloriesString = $"{hikeActivity.Calories} kcal";
				KmString = $"{hikeActivity.DistanceInKM} km";
			}
            else if (Activity is RunActivity runActivity)
            {
				double speed = Math.Truncate(runActivity.AvgSpeed * 100) / 100;
				AverageVelocityString = $"{speed} km/h";
				CaloriesString = $"{runActivity.Calories} kcal";
				KmString = $"{runActivity.DistanceInKM} km";
			}

			OnPropertyChanged(nameof(DateString));
			OnPropertyChanged(nameof(TimeString));
			OnPropertyChanged(nameof(KmString));
			OnPropertyChanged(nameof(DurationString));
			OnPropertyChanged(nameof(AverageVelocityString));
			OnPropertyChanged(nameof(CaloriesString));

			return;

		}

        public ActivityDetailsViewModel()
		{
		}

        private Command closePageCommand;
        public ICommand ClosePageCommand => closePageCommand ??= new Command(ClosePage);

        private void ClosePage()
        {
			//Shell.Current.CurrentItem.CurrentItem = Shell.Current.CurrentItem.Items[0];
			//Shell.Current.GoToAsync($"{nameof(DashboardPage)}");
			//await Shell.Current.GoToAsync($"{nameof(ActivityPage)}");
			//Application.Current.MainPage.Navigation.PopAsync();
			//Shell.Current.CurrentItem.CurrentItem = Shell.Current.CurrentItem.Items[0];
			Shell.Current.GoToAsync("..");
		}
    }
}

