﻿using System;
using System.Collections.Generic;
using System.IO;
using TrackingApp.Models;
using TrackingApp.ViewModels;
using Xamarin.Forms;

namespace TrackingApp.Views
{	
	public partial class ActivityPage : ContentPage
	{	
		public ActivityPage ()
		{
			InitializeComponent ();
            BindingContext = new ActivityViewModel();
		}

        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var activityType = ((ListView)sender).SelectedItem as ActivityType;
            if (activityType != null)
            {
                bool answer = await DisplayAlert("Aufzeichnen?", "Möchtest du eine Aktivität Aufzeichnen oder Nacherfassen?", "Aufzeichnen", "Nacherfassen");
  
                if (answer)
                {
                    //await DisplayAlert("Fehlt noch", "Diese Funktion ist noch nicht implementiert!", "OK");
                    await Shell.Current.GoToAsync($"{nameof(ActivityRecordPage)}?activityTypeId={activityType.Id}");
                } else
                {
                    await Shell.Current.GoToAsync($"{nameof(ActivityViewPage)}?activityTypeId={activityType.Id}");
                }
                
            }
        }

        private void ListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }




        //private string activityIconB64;
        //public string ActivityIconB64
        //{
        //    get { return activityIconB64; }
        //    set
        //    {
        //        activityIconB64 = value;
        //        OnPropertyChanged("ImageBase64");

        //        Image = ImageSource.FromStream(
        //            () => new MemoryStream(Convert.FromBase64String(activityIconB64)));
        //    }
        //}

        //private ImageSource image;
        //public ImageSource Image
        //{
        //    get { return image; }
        //    set
        //    {
        //        image = value;
        //        OnPropertyChanged("Image");
        //    }
        //}

    }
}

