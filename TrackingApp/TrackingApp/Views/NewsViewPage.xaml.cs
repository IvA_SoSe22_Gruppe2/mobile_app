﻿using System;
using System.Collections.Generic;
using TrackingApp.Models;
using TrackingApp.Models.Ressources;
using TrackingApp.ViewModels;
using Xamarin.Forms;

namespace TrackingApp.Views
{	
	public partial class NewsViewPage : ContentPage
	{	
		public NewsViewPage ()
		{
			InitializeComponent ();
            BindingContext = new NewsViewViewModel();
		}


    }
}

