﻿using System;
using System.Collections.Generic;
using Microcharts;
using TrackingApp.Models;
using TrackingApp.Models.UserActivities;
using TrackingApp.Services;
using Xamarin.Forms;
using Entry = Microcharts.ChartEntry;

namespace TrackingApp.Views
{
	public partial class DashboardPage : ContentPage
	{
        //public List<Entry> entries = new List<Entry>();
        //{
			/*new Entry(4)
			{
				Label = "Test1",
				ValueLabel = "",
				Color = SkiaSharp.SKColor.Parse("#00BFFF"),
			},
			new Entry(6)
			{
				Label = "Test2",
				ValueLabel = "",
				Color = SkiaSharp.SKColor.Parse("#00BFFF"),
			},*/
			
		//};

		public DashboardPage ()
		{
			InitializeComponent();
            //Chart1.Chart = new DonutChart() { Entries = entries };
        }

        // When User clicks News Item in List: navigate to NewsViewPage for detailed view of selected news item
        private async void NewsListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var news = ((ListView)sender).SelectedItem as News;
            if (news != null)
            {
                await Shell.Current.GoToAsync($"{nameof(NewsViewPage)}?newsId={news.Id}");
            }
        }

        private void NewsListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }
        private async void Activities_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (((ListView)sender).SelectedItem is UserActivity activity)
            {
                await Shell.Current.GoToAsync($"{nameof(ActivityDetailsPage)}?activityId={activity.Id}");
            }
        }

        private void Activities_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            ((ListView)sender).SelectedItem = null;
        }
    }
}

