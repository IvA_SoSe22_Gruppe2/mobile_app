﻿using System;
using System.Collections.Generic;
using TrackingApp.Views;
using Xamarin.Forms;

namespace TrackingApp
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            //Routing.RegisterRoute(nameof(LoginPage), typeof(LoginPage));
            Routing.RegisterRoute(nameof(DashboardPage), typeof(DashboardPage));
            Routing.RegisterRoute(nameof(ActivityPage), typeof(ActivityPage));
            Routing.RegisterRoute(nameof(ActivityViewPage), typeof(ActivityViewPage));
            Routing.RegisterRoute(nameof(ProfilPage), typeof(ProfilPage));
            Routing.RegisterRoute(nameof(StudyActivityPage), typeof(StudyActivityPage));
            Routing.RegisterRoute("//LoginPage/ForgotPasswordPage", typeof(ForgotPasswordPage));

            //Code from here is temp and not final:
            Routing.RegisterRoute(nameof(NewsViewPage), typeof(NewsViewPage));
            Routing.RegisterRoute(nameof(ActivityDetailsPage), typeof(ActivityDetailsPage));
            Routing.RegisterRoute(nameof(ActivityRecordPage), typeof(ActivityRecordPage));
        }
    }
}

