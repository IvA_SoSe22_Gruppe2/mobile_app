﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TrackingApp.Models;
using TrackingApp.Models.UserActivities;
using TrackingApp.ViewModels;
using Xamarin.Forms;
using System.Security.Cryptography;
using Newtonsoft.Json.Linq;
using TrackingApp.Models.Ressources;

namespace TrackingApp.Services
{
    public class RestService
    {
        readonly HttpClient _client;
        readonly string uri = Constants.AzureApiUrl;

        

        public RestService()
        {
            _client = new HttpClient();

            if (Device.RuntimePlatform == Device.UWP)
            {
                //_client.DefaultRequestHeaders.Add("User-Agent", ".NET Foundation Repository Reporter");
                //_client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/vnd.github.v3+json"));
            }
        }

        public async Task<User> GetUserByID( int id)
        {
            User user = null;
            try
            {
                Console.WriteLine($"{uri}/User/{id}");
                HttpResponseMessage response = await _client.GetAsync($"{uri}/User/{id}");
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    user = JsonConvert.DeserializeObject<User>(content.Substring(1,content.Length-2));
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\tERROR {0}", ex.Message);
            }

            return user;
        }

        public async Task<User> PatchUser()
        {
            User user = Constants.GlobalUser;
            try
            {
                Console.WriteLine($"{uri}/User/{user.Id}");

                string package = $"{GetUserJSONWithoutID()}";

                var request = new HttpRequestMessage(new HttpMethod("PATCH"), $"{uri}/User/App/{user.Id}")
                {
                    
                Content = new StringContent(package, Encoding.UTF8, "application/json-patch+json")
                
                };
                HttpResponseMessage response = await _client.SendAsync(request);
                response.EnsureSuccessStatusCode();
               
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\tERROR {0}", ex.Message);
            }

            return await GetUserByID(user.Id);
        }

        private string GetUserJSONWithoutID()
        {
           
            User user = Constants.GlobalUser;
            string json = JsonConvert.SerializeObject(user);
            json = "{"+json.Substring(json.IndexOf(',') + 1);
            Console.WriteLine(json);
            return json;
        }



        public async Task<int> LoginUser(string email, string password)
        {
            string hashedpwd = "";
            using (Aes myAes = Aes.Create())
            {
                // Generate AES Key (256) and IV 
                byte[] Key = Encoding.ASCII.GetBytes(@"qTr{@^h`F&_`90/ja9!'dcmh3!uw?&=?");
                byte[] IV = Encoding.ASCII.GetBytes(@"4&\~V(,A.l-&Pf65");
                myAes.Key = Key;
                myAes.IV = IV;
                // Encrypt the string to an array of bytes.
                byte[] encrypted = EncryptStringToBytes_Aes(password, myAes.Key, myAes.IV);
                // Decrypt the bytes to a string.
                // string roundtrip = DecryptStringFromBytes_Aes(encrypted, myAes.Key, myAes.IV);

                hashedpwd = BitConverter.ToString(encrypted).Replace("-", "").ToLower();

                // Console.WriteLine("Round Trip: {0}", roundtrip);
            }

            int result = -1;
            try
            {
                Console.WriteLine($"{uri}/User/loginValidation?email={email}&password={hashedpwd}");
                HttpResponseMessage response = await _client.GetAsync($"{uri}/User/loginValidation?email={email}&password={hashedpwd}");
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    User user = JsonConvert.DeserializeObject<User>(content.Substring(1, content.Length - 2));
                    result = user.Id;
                    if (result > 0)
                    {
                        Console.WriteLine("Login Korrekt. Hole Daten von User mit ID " + result);
                        Constants.GlobalUser = user;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\tERROR {0}", ex.Message);
            }

            return result;
        }

        public async Task<ObservableCollection<ActivityType>> GetAllActivityTypes()
        {
            ObservableCollection<ActivityType> ActivityTypes = null;
            try
            {
                Console.WriteLine($"{uri}/Activity/getAllActivities");
                HttpResponseMessage response = await _client.GetAsync($"{uri}/Activity/getAllActivities");
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    ActivityTypes = JsonConvert.DeserializeObject<ObservableCollection<ActivityType>>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\tERROR {0}", ex.Message);
            }

            return ActivityTypes;
        }

        public async Task<ObservableCollection<UserActivity>> GetAllActivities()
        {
            ObservableCollection<UserActivity> activities = new ObservableCollection<UserActivity>();
            try
            {
                Console.WriteLine($"{uri}/Event/getAllEventsByUserID/{Constants.GlobalUser.Id}");
                HttpResponseMessage response = await _client.GetAsync($"{uri}/Event/getAllEventsByUserID/{Constants.GlobalUser.Id}");
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    JArray arr = JArray.Parse(await response.Content.ReadAsStringAsync());
                    foreach (JObject obj in arr.Children<JObject>())
                    {
                        int activityId = (int)obj.GetValue("activityId");

                       switch (activityId)
                        {
                            case 2:
                                {
                                    HikeActivity hikeActivity = obj.ToObject<HikeActivity>();
                                    activities.Add(hikeActivity); 
                                    break;
                                }
                            case 1:
                                {
                                    RunActivity runActivity = obj.ToObject<RunActivity>();
                                    activities.Add(runActivity);
                                    break;
                                }
                            case 5:
                                {
                                    SleepActivity sleepActivity = obj.ToObject<SleepActivity>();
                                    activities.Add(sleepActivity);
                                    break;
                                }
                            case 4:
                                {
                                    WorkActivity workActivity = obj.ToObject<WorkActivity>();
                                    activities.Add(workActivity);
                                    break;
                                }
                            case 3:
                                {
                                    StudyActivity studyActivity = obj.ToObject<StudyActivity>();
                                    activities.Add(studyActivity);
                                    break;
                                }
                            default:
                                {
                                    throw new ActivityTypeNotFoundException(activityId);
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\tERROR {0}", ex.Message);
                await Application.Current.MainPage.DisplayAlert("Fehler", ex.Message, "OK");
            }

            return activities;
        }

        public async Task<bool> ForgotPasswordAsync(string eMail)
        {
            try
            {
                string package = $"{{\"email\": \"{eMail}\"}}";
                var content = new StringContent(package, Encoding.UTF8, "application/json-patch+json");
                HttpResponseMessage response = await _client.PostAsync($"{uri}/user/forgotPassword?email={eMail}", content);
                response.EnsureSuccessStatusCode();

            }
            catch (Exception ex)
            {
                Debug.WriteLine("\tERROR {0}", ex.Message);
                return false;
            }

            return true;
        }

        public async Task<bool> AddActivitiy(PreparedActivity activity)
        {
            try
            {
                string package = JsonConvert.SerializeObject(activity);
                package = CutParamsActivityIDUserID(package);
                var content = new StringContent(package, Encoding.UTF8, "application/json-patch+json");
                Console.WriteLine($"{uri}/Event?activityId={activity.TypeId}&userId={Constants.GlobalUser.Id}");
                HttpResponseMessage response = await _client.PostAsync($"{uri}/Event?activityId={activity.TypeId}&userId={Constants.GlobalUser.Id}",content);
                response.EnsureSuccessStatusCode();
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\tERROR {0}", ex.Message);
                return false;
            }

            return true;
        }

        private string CutParamsActivityIDUserID(string json)
        {
            json = "{" + json.Substring(json.IndexOf(',') + 1);
            json = "{" + json.Substring(json.IndexOf(',') + 1);
            Console.WriteLine(json);
            return json;
        }

        public async Task<ObservableCollection<News>> GetAllNews()
        {
            ObservableCollection<News> newslist = null;
            try
            {
                Console.WriteLine($"{uri}/News/getAllNews");
                HttpResponseMessage response = await _client.GetAsync($"{uri}/News/getAllNews");
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    newslist = JsonConvert.DeserializeObject<ObservableCollection<News>>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("\tERROR {0}", ex.Message);
            }

            return newslist;
        }

        static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;

            // Create Aes object with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create an encryptor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using MemoryStream msEncrypt = new MemoryStream();
                using CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write);
                using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                {
                    //Write all data to the stream.
                    swEncrypt.Write(plainText);
                }
                encrypted = msEncrypt.ToArray();
            }
            // Return the encrypted bytes from the memory stream.
            return encrypted;
        }

        //static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        //{
        //    // Check arguments.
        //    if (cipherText == null || cipherText.Length <= 0)
        //        throw new ArgumentNullException("cipherText");
        //    if (Key == null || Key.Length <= 0)
        //        throw new ArgumentNullException("Key");
        //    if (IV == null || IV.Length <= 0)
        //        throw new ArgumentNullException("IV");

        //    // Declare the string used to hold
        //    // the decrypted text.
        //    string plaintext = null;

        //    // Create an Aes object
        //    // with the specified key and IV.
        //    using (Aes aesAlg = Aes.Create())
        //    {
        //        aesAlg.Key = Key;
        //        aesAlg.IV = IV;

        //        // Create a decryptor to perform the stream transform.
        //        ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

        //        // Create the streams used for decryption.
        //        using (MemoryStream msDecrypt = new MemoryStream(cipherText))
        //        {
        //            using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
        //            {
        //                using (StreamReader srDecrypt = new StreamReader(csDecrypt))
        //                {

        //                    // Read the decrypted bytes from the decrypting stream
        //                    // and place them in a string.
        //                    plaintext = srDecrypt.ReadToEnd();
        //                }
        //            }
        //        }
        //    }

        //    return plaintext;
        //}

    }
}
