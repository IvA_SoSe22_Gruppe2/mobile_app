﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Text;
using TrackingApp.Models;
using TrackingApp.Models.UserActivities;

namespace TrackingApp.Services
{
    public static class Constants
    {
        //public const string GitHubReposEndpoint = "https://api.github.com/orgs/dotnet/repos";
        public const string AzureApiUrl = "https://ivanew2.azurewebsites.net/api";
        public static User GlobalUser { get; set; }
        public static ObservableCollection<News> NewsList { get; set; }
        public static ObservableCollection<ActivityType> ActivityTypes { get; set; }
        public static ObservableCollection<UserActivity> Activities { get; set; }

        public static double AverageCalories { get; set; }
        public static double AverageSpeed { get; set; }
        public static int[] AbsoluteAmountsOfTrainingsByActivityType { get; set; }

        public static CultureInfo CultureInfo => new CultureInfo("de-DE");

        public static ActivityType GetActivityTypeByID(int activityId)
        {
            foreach (ActivityType activityType in Constants.ActivityTypes)
            {
                if (activityType.Id == activityId) return activityType;
            }
            throw new ActivityTypeNotFoundException(activityId);
        }

        public static void LoadDiagrammData()
        {
            AverageCalories = CalculateAverageCalories();
            AverageSpeed = CalculateAverageSpeed();
            AbsoluteAmountsOfTrainingsByActivityType = CalculateAbsoluteAmountsOfTrainingsByActivityType();
        }

        private static int[] CalculateAbsoluteAmountsOfTrainingsByActivityType()
        {
            int[] result = new int[ActivityTypes.Count];

            foreach (UserActivity activity in Activities)
            {
                result[activity.ActivityType.Id-1]++;
            }

            return result;
        }

        private static double CalculateAverageSpeed()
        {
            double totalDistance = 0;
            double totalHours = 0;

            foreach (UserActivity activity in Activities)
            {
                if (activity.ActivityType.Name == "Laufen")
                {
                    RunActivity calorieActivity = activity as RunActivity;
                    if (calorieActivity.Calories >= 0)
                    {
                        totalDistance += calorieActivity.DistanceInKM;
                        totalHours += calorieActivity.Duration.TotalHours;
                    }
                }
                else continue;
            }
            if (totalHours == 0) return 0;
            else return totalDistance / totalHours;
        }

        public static double CalculateAverageCalories()
        {
            double result = 0;
            int counter = 0;

            foreach (UserActivity activity in Activities)
            {
                if (activity.ActivityType.Name == "Laufen")
                {
                    RunActivity calorieActivity = activity as RunActivity;
                    if (calorieActivity.Calories >= 0)
                    {
                        result += calorieActivity.Calories;
                        counter++;
                    }
                }
                else if (activity.ActivityType.Name == "Wandern")
                {
                    HikeActivity calorieActivity = activity as HikeActivity;
                    if (calorieActivity.Calories >= 0)
                    {
                        result += calorieActivity.Calories;
                        counter++;
                    }
                }
                else continue;
            }
            if (counter == 0) return 0;
            else return result/counter;
        }



    }
}
